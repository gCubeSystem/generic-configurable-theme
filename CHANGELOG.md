
# Changelog for D4Science Generic Liferay Theme

All notable changes to this project will be documented in this file.
This project does not adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v6.6.2-SNAPSHOT] - 2023-08-31

- Updated Funded by EU logo


## [v6.6.1] - 2022-12-01

- updated logo

## [v6.6.0] - 2022-03-17

- Added automatic request support URL when none is specified in the setting

## [v6.5.0] - 2021-12-17

- Added possibility to customise feedback container URL and Label (Report an issue) and its visibility via a checkbox

## [v6.4.0-SNAPSHOT] - 2017-01-24

 - First release

